<?php
/**
 * Class ClassTaxonomyRegister
 *
 * @package WPezSuite\WPezClasses\TaxonomyRegister
 */

namespace WPezSuite\WPezClasses\TaxonomyRegister;

// No WP? No good.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

use \WPezSuite\WPezClasses\TaxonomyRegister\InterfaceTaxonomyRegister;


/**
 * Push and register taxonomies.
 */
class ClassTaxonomyRegister implements InterfaceTaxonomyRegister {

	/**
	 * Array of taxonomy(s) w/ object_types and args to be registers.
	 *
	 * @var array
	 */
	protected $arr_taxs;

	/**
	 * Results of the register_taxonomy() key'ed by taxonomy name.
	 *
	 * @var array
	 */
	protected $arr_ret;

	/**
	 * Class constructor.
	 */
	public function __construct() {

		$this->setPropertyDefaults();
	}

	/**
	 * Sets the defaults of the class' property's.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_taxs = array();
		$this->arr_ret  = array();
	}

	/**
	 * Push a taxonomy onto the array of taxs to be registered
	 *
	 * @param string $str_taxonomy    Taxonomy name.
	 * @param array  $arr_object_type Object type(s) to register the taxonomy to.
	 * @param array  $arr_args        Args for the eventual register_taxonomy().
	 *
	 * @return false|string
	 */
	public function pushTaxonomy( string $str_taxonomy = '', array $arr_object_type = array(), array $arr_args = array() ) {

		if ( ! empty( $str_taxonomy ) ) {

			$str_taxonomy = strtolower( $str_taxonomy );

			// Only allow a-z, 0 - 9 and _, else remove other characters.
			// @link https:// developer.wordpress.org/reference/functions/sanitize_key/ .
			$str_taxonomy = sanitize_key( $str_taxonomy );

			if ( strlen( $str_taxonomy ) < 33 ) {

				$this->arr_taxs[ $str_taxonomy ] = array(
					'object_type' => $arr_object_type,
					'args'        => $arr_args,
				);

				// return the tax name registered (as sanitize_key might have changed it).
				return $str_taxonomy;
			}
		}
		return false;
	}


	/**
	 * Bulk load an array of multiple taxonomies.
	 *
	 * @param array $arr_taxs Array of arrays (of tax_name => array ( object_type, args ) ) that'll be pushTaxonomy()'ed one by one.
	 *
	 * @return array|false
	 */
	public function loadTaxonomies( array $arr_taxs = array() ) {

		if ( ! empty( $arr_taxs ) ) {
			$arr_ret = array();
			foreach ( $arr_taxs as $str_tx => $arr_args ) {

				if ( ! isset( $arr_args['object_type'] ) || ! isset( $arr_args['args'] ) ) {
					continue;
				}

				$arr_ret[ $str_tx ] = $this->pushTaxonomy( $str_tx, $arr_args['object_type'], $arr_args['args'] );

			}
			return $arr_ret;
		}
		return false;
	}

	/**
	 * Registers any/all taxs in the property: $arr_taxs.
	 *
	 * @return void
	 */
	public function registerTaxonomies() {

		foreach ( $this->arr_taxs as $str_tx => $arr_args ) {

			$this->arr_ret[ $str_tx ] = register_taxonomy( $str_tx, $arr_args['object_type'], $arr_args['args'] );
		}
	}

	/**
	 * Returns the property: $arr_ret.
	 *
	 * @return array
	 */
	public function getReturn() {

		return $this->arr_ret;
	}
}
