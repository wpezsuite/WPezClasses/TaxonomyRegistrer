<?php
/**
 * Does the necessary WP actions and filters to register a taxonomy. This is optional but also helps to make things ez.
 *
 * @package WPezSuite\WPezClasses\TaxonomyRegister
 */

namespace WPezSuite\WPezClasses\TaxonomyRegister;

// No WP? No good.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * Class ClassHooks
 */
class ClassHooks {

	/**
	 * An instance of a class that implements InterfaceTaxonomyRegister.
	 *
	 * @var InterfaceTaxonomyRegister
	 */
	protected $new_component;

	/**
	 * The default values for the hooks.
	 *
	 * @var array
	 */
	protected $arr_hook_defaults;

	/**
	 * The actions to be registered.
	 *
	 * @var array
	 */
	protected $arr_actions;

	/**
	 * Class constructor.
	 *
	 * @param InterfaceTaxonomyRegister $obj An instance of a class that implements InterfaceTaxonomyRegister.
	 */
	public function __construct( InterfaceTaxonomyRegister $obj ) {

		$this->new_component = $obj;
		$this->setPropertyDefaults();
	}

	/**
	 * Set the class property defaults.
	 */
	protected function setPropertyDefaults() {

		$this->arr_hook_defaults = array(
			'active'        => true,
			'component'     => $this->new_component,
			'priority'      => '10',
			'accepted_args' => '1',
		);

		$this->arr_actions = array();

		$this->arr_actions['register'] = array(
			'hook'     => 'init',
			'callback' => 'registerTaxonomies',
		);
	}

	/**
	 * Setter to update the property: arr_hook_defaults.
	 *
	 * @param array $arr Array of values to be merged over the hook defaults.
	 *
	 * @return bool
	 */
	public function updateHookDefaults( array $arr = array() ) {

		if ( ! empty( $arr ) ) {

			$this->arr_hook_defaults = array_merge( $this->arr_hook_defaults, $arr );
			return true;
		}
		return false;
	}

	/**
	 * Getter for the property: arr_actions.
	 *
	 * @return array
	 */
	public function getActions() {

		return $this->arr_actions;
	}

	/**
	 * Setter to update the property: arr_actions.
	 *
	 * @param array $arr Array of values to be merged over the actions.
	 *
	 * @return bool
	 */
	public function updateActions( array $arr = array() ) {

		if ( ! empty( $arr ) ) {

			$this->arr_actions = array_merge( $this->arr_actions, $arr );
			return true;
		}
		return false;
	}

	/**
	 * Register the actions with WordPress.
	 *
	 * @param array $arr_exclude Array of actions to exclude, a failsafe of sorts.
	 */
	public function register( array $arr_exclude = array() ) {

		foreach ( $this->arr_actions as $str_ndx => $arr_action ) {

			if ( in_array( $str_ndx, $arr_exclude, true ) ) {
				continue;
			}

			$arr = array_merge( $this->arr_hook_defaults, $arr_action );
			if ( false === $arr['active'] ) {
				continue;
			}

			add_action(
				$arr['hook'],
				array( $arr['component'], $arr['callback'] ),
				$arr['priority'],
				$arr['accepted_args']
			);
		}
	}
}
