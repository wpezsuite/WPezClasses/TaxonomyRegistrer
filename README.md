## WPezClasses: Taxonomy Register

__Register your WordPress custom taxonomies The ezWay.__

Along with TaxonomyLabels and TaxonomyArgs, the circle is now complete.
   

### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._


    use WPezSuite\WPezClasses\TaxonomyLabelsUS\ClassTaxonomyLabelsUS as Labels;
    use WPezSuite\WPezClasses\TaxonomyArgs\ClassTaxonomyArgs as TaxArgs;
    use WPezSuite\WPezClasses\TaxonomyRegister\ClassTaxonomyRegister as TaxReg;
    use WPezSuite\WPezClasses\TaxonomyRegister\ClassHooks as Hooks;
    
    $new_labels = new Labels();
    $new_labels->setNames('TEST Tax');
    $arr_labels = $new_labels->getLabels();
    
    $new_args = new TaxArgs();
    $new_args->setLabels( $arr_labels );
    $new_args->setArgs([...]); // Or use the individual setters.
    $arr_args = $new_args->getArgsAll();
    
    $new_reg = new TaxReg
    $new_reg->pushTaxonomy( 'my_test_tax', array( object_type(s) ), $arr_args );
    
    $new_hooks = new Hooks( $new_reg );
    $new_hooks->register();
    
    



### FAQ

__1) Why?__

Truth be told, you could take the args and do your own register_post_type(). However, The ezWay eliminates having to know that hook, create that function / method, etc. What's normally many lines of code has been reduced to 10 - 15 lines of simplified configuring. 

Less thinking and typing, more doing; such is The ezWay. 


__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

- https://gitlab.com/wpezsuite/WPezClasses/WPezAutoload

- https://gitlab.com/wpezsuite/WPezClasses/TaxonomyLabelsUS

- https://gitlab.com/wpezsuite/WPezClasses/TaxonomyArgs

- https://codex.wordpress.org/Function_Reference/register_post_type



### TODO



### CHANGE LOG

-v.0.0.3 - Saturday 7 Jan 2023
    - UPDATE: Refactoring + comments of the three WPez repos for WP taxonomies.

- v0.0.2 - Monday 22 April 2019
    - UPDATED: interface file / name

- v0.0.1 - Saturday 20 April 2019
    - This has been in development for quite some time, but not independently repo'ed until now. Please pardon the delay. 