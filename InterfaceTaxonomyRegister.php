<?php
/**
 * Interface InterfaceTaxonomyRegister
 *
 * @package WPezSuite\WPezClasses\TaxonomyRegister
 */

namespace WPezSuite\WPezClasses\TaxonomyRegister;

/**
 * Interface InterfaceTaxonomyRegister
 */
interface InterfaceTaxonomyRegister {

	/**
	 * Register the taxonomy(s).
	 */
	public function registerTaxonomies();

}
